<div class="footer">
    <p>Copyright © <?=date('Y')?></p>
    <p><?=bloginfo('name')?></p>
</div>
<div class="pic-info">
    <span class="info-container">i</span>
    <div class="info-popover">
        <span class="label">Photographer</span>
        <span class="answer">Tim Trad</span>
        <span class="label">Location</span>
        <span class="answer">Skógar, Garðabær, Iceland</span>
        <span class="label">Publish Date</span>
        <span class="answer">January 21, 2017</span>
        <span class="label">License</span>
        <span class="answer"><a href="http://creativecommons.org/publicdomain/zero/1.0/" target="_blank" title="Link to CC0 1.0 License">CC0 1.0</a></span>
        <span class="label">Image URL</span>
        <span class="answer"><a href="https://unsplash.com/photos/2gk6BDXSxlQ" target="_blank" title="Link to background image on Unsplash">Unsplash</a></span>
    </div>
</div>

<?=wp_footer()?>

</body>
</html>
