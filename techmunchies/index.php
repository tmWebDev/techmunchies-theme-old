<?=get_header()?>
<div class="content">
    <div class="site-name" data-letters="techmunchies"></div>
    <p>Traverse New Roads</p>
    <p>Coming Soon, A Development &amp; Design Company</p>
    <!--<?=do_shortcode('[contact-form-7 id="72" title="Contact Me"]')?>-->
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/ee26ed5786.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/segment-js/1.0.8/segment.min.js" integrity="sha256-YoxPyNrFgtt/RkvUUU9rKTG7y68/DkDRUvgd1Hus4qY=" crossorigin="anonymous"></script>
<script type="text/javascript">$crisp=[];CRISP_WEBSITE_ID="2e2ea482-b7b4-4c32-976d-874a3efac439";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.im/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
<?=get_footer()?>
