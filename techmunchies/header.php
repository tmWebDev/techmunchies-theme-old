<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?=language_attributes()?>>
<head profile="http://gmpg.org/xfn/11">
<title><?=bloginfo('name')?> <?=wp_title()?></title>
<meta http-equiv="Content-Type" content="<?=bloginfo('html_type')?>; charset=<?=bloginfo('charset')?>" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
<?=wp_head()?>
</head>
<body>
